
<p align="center">
    <img src="/readme/images/logo.png" style="width:100px;"/>
</p>
<h1 align="center"> pcfcms企业建站系统</h1> 
<p align="center">    
    <b>如果对您有帮助，您可以点右上角 "Star" 支持一下 谢谢！</b>
</p>

## 项目介绍
   pcfcms是基于TP6.0框架为核心开发的免费+开源的企业内容管理系统，专注企业建站用户需求提供海量各行业模板，降低中小企业网站建设、网络营销成本，致力于打造用户舒适的建站体验

### 导航栏目
[使用手册](https://www.kancloud.cn/pancaofu/pcfcms_doc/2634106)
 | [安装说明](https://www.kancloud.cn/pancaofu/pcfcms_doc/2634107)
 | [官网地址](http://www.pcfcms.com)
 | [服务器](http://www.pcfcms.com/host.html)
 | [授权价格](http://www.pcfcms.com/grant.html)
- - -

#### 应用初始化
1.  打开命令工具cmd.bat
2.  运行composer install

#### 安装教程
1.  默认目录运行public
2.  php版本7.2-7.4
3.  开启伪静态和安装扩展fileinfo

####   :fire:  演示站后台:[<a href='http://demo.pcfcms.com' target="_blank"> 查看 </a>]       
<a href='http://demo.pcfcms.com/login.php' target="_blank">http://demo.pcfcms.com/login.php</a>  账号：admin  密码：123456


### 功能图解
![输入图片说明](readme/images/%E5%8A%9F%E8%83%BD%E5%9B%BE%E8%A7%A3.jpg)

### 页面展示
![输入图片说明](readme/images/PCFCMS-%E5%90%8E%E5%8F%B0%E7%99%BB%E5%BD%95.png)
![输入图片说明](readme/images/PCFCMS%20%E5%90%8E%E5%8F%B0%E7%AE%A1%E7%90%86%E5%BC%80%E5%8F%91%E6%A1%86%E6%9E%B6.png)
![输入图片说明](readme/images/PCFCMS%20%E5%90%8E%E5%8F%B0%E8%BD%AE%E6%92%AD%E5%9B%BE.png)
![输入图片说明](readme/images/PCFCMS%20%E5%90%8E%E5%8F%B0%E6%A8%A1%E6%9D%BF%E7%AE%A1%E7%90%86.png)
![输入图片说明](readme/images/PCFCMS%20%E5%90%8E%E5%8F%B0%E6%96%87%E6%A1%A3%E5%86%85%E5%AE%B9%E5%88%97%E8%A1%A8.png)
![输入图片说明](readme/images/PCFCMS%20%E5%90%8E%E5%8F%B0%E6%95%B0%E6%8D%AE%E5%BA%93%E7%AE%A1%E7%90%86.png)
![输入图片说明](readme/images/PCFCMS%20%E5%90%8E%E5%8F%B0%E6%8F%92%E4%BB%B6%E4%B8%AD%E5%BF%83.png)
![输入图片说明](readme/images/PCFCMS%20%E5%90%8E%E5%8F%B0%E5%A4%9A%E8%AF%AD%E8%A8%80.png)
![输入图片说明](readme/images/PCFCMS%20%E5%90%8E%E5%8F%B0%E4%BC%9A%E5%91%98%E4%B8%AD%E5%BF%83.png)

# 特别鸣谢
感谢以下的项目,排名不分先后

ThinkPHP：http://www.thinkphp.cn

layui: https://www.layui.com

### 版权信息
本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © by PCFCMS (http://www.pcfcms.com)

All rights reserved。